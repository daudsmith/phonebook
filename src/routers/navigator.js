import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ListContact from '../screens/listContact';
import AddContact from '../screens/addContact';
import EditContact from '../screens/editContact';

const Stack = createStackNavigator();

const MainNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}} initialRouteName="List Contact">
        <Stack.Screen name="List Contact" component={ListContact}/>
        <Stack.Screen name="Add Contact" component={AddContact}/>
        <Stack.Screen name="Edit Contact" component={EditContact}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MainNavigator;
