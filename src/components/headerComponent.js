import React from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';

const HeaderComponent = () => {

    return (
        <View style={styles.header}>
            <Text style={styles.headerText}>Phonebook</Text>
        </View>
    );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#3498db',
    padding: 20,
    alignItems: 'center',
    marginBottom: 8,
},
headerText: {
    color: '#fff',
    fontSize: 19,
    fontWeight: 'bold',
},
});

export default HeaderComponent;
