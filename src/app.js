import React from 'react';
import MainNavigator from './routers/navigator';
import { ContactProvider } from './context/contactContext'

const App = () => {
  return (
    <ContactProvider>
      <MainNavigator />
    </ContactProvider>
  );
};

export default App;