import React, { useContext, useEffect, useState } from 'react';
import { SafeAreaView, TextInput, TouchableOpacity, View, Text, StyleSheet, Image } from 'react-native';
import { launchImageLibrary } from 'react-native-image-picker';
import LoadingComponent from '../components/loadingComponent';
import HeaderComponent from '../components/headerComponent';
import { ContactContext } from '../context/contactContext';
import { styles } from '../styles/style';


const AddContact = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(true);
  const { data, setData } = useContext(ContactContext);
  const [imageSource, setImageSource] = useState('');
  const [fullname, setFullname] = useState('');
  const [phonenumber, setPhonenumber] = useState('');

  const handleFullnameChange = (fullname) => {
    setFullname(fullname);
  };

  const handlePhoneChange = (phonenumber) => {
    setPhonenumber(phonenumber);
  };

  const selectImage = () => {
    launchImageLibrary({ title: 'Select Image' }, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        setImageSource(response.assets[0].uri);
      }
    });
  };

  const handleSaveButtonPress = () => {
    const newId = data.length > 0 ? Math.max(...data.map(item => item.id)) + 1 : 1;
    const newData = {
      'id': newId,
      'fullname': fullname,
      'phonenumber': phonenumber,
      'imageSource': imageSource
    }
    setData(prevData => [...prevData, newData]);
    navigation.navigate('List Contact');
  };

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 300);
  }, []);

  if (isLoading) {
    return (
      <LoadingComponent/>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
        <HeaderComponent/>
        <View style={styles.container}>
          {imageSource !== '' ? (
            <Image source={{ uri: imageSource }} style={styles.avatarPlaceholderUpload} />
          ) : (
            <View style={styles.avatarPlaceholderUpload}>
              <TouchableOpacity onPress={selectImage}>
                <Text style={styles.uploadText}>Upload Image</Text>
              </TouchableOpacity>
            </View>
          )}
              <TextInput
                  style={styles.input}
                  value={fullname}
                  onChangeText={handleFullnameChange}
                  placeholder="Fullname"
              />
              <TextInput
                  keyboardType="numeric"
                  style={styles.input}
                  value={phonenumber}
                  onChangeText={handlePhoneChange}
                  placeholder="Phone Number"
              />
            <TouchableOpacity style={styles.saveButton} onPress={handleSaveButtonPress}>
                <Text style={styles.saveButtonText}>Save</Text>
            </TouchableOpacity>
        </View>
    </SafeAreaView>
  );
};

export default AddContact;