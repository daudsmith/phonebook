import React, { useContext, useEffect, useState } from 'react';
import { SafeAreaView, TextInput, TouchableOpacity, View, Text, StyleSheet, Image } from 'react-native';
import { launchImageLibrary } from 'react-native-image-picker';
import LoadingComponent from '../components/loadingComponent';
import HeaderComponent from '../components/headerComponent';
import { ContactContext } from '../context/contactContext';
import { styles } from '../styles/style';


const EditContact = ({ route, navigation }) => {
  const [isLoading, setIsLoading] = useState(true);
  const { data, setData } = useContext(ContactContext);
  const { contact } = route.params;
  const [imageSource, setImageSource] = useState(contact.imageSource);
  const [fullname, setFullname] = useState(contact.fullname);
  const [phonenumber, setPhonenumber] = useState(contact.phonenumber);

  const handleFullnameChange = (fullname) => {
    setFullname(fullname);
  };

  const handlePhoneChange = (phonenumber) => {
    setPhonenumber(phonenumber);
  };

  const selectImage = () => {
    launchImageLibrary({ title: 'Select Image' }, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        setImageSource(response.assets[0].uri);
      }
    });
  };

  const handleSaveButtonPress = () => {
    const updatedData = data.map(item => {
      if (item.id === contact.id) {
        return {
          ...item,
          fullname,
          phonenumber,
          imageSource
        };
      }
      return item;
    });

    setData(updatedData);
    navigation.navigate('List Contact');
  };

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 500);
  }, []);

  if (isLoading) {
    return (
      <LoadingComponent/>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
        <HeaderComponent/>
        <View style={styles.container}>
          {imageSource !== '' ? (
            <Image source={{ uri: imageSource }} style={styles.avatarPlaceholderUpload} />
          ) : (
            <View style={styles.avatarPlaceholderUpload}>
              <TouchableOpacity onPress={selectImage}>
                <Text style={styles.uploadText}>Upload Image</Text>
              </TouchableOpacity>
            </View>
          )}
              <TextInput
                  style={styles.input}
                  value={fullname}
                  onChangeText={handleFullnameChange}
                  placeholder="Fullname"
              />
              <TextInput
                  keyboardType="numeric"
                  style={styles.input}
                  value={phonenumber}
                  onChangeText={handlePhoneChange}
                  placeholder="Phone Number"
              />
            <TouchableOpacity style={styles.saveButton} onPress={handleSaveButtonPress}>
                <Text style={styles.saveButtonText}>Save</Text>
            </TouchableOpacity>
        </View>
    </SafeAreaView>
  );
};
export default EditContact;