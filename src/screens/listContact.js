import React, { useContext, useEffect, useState } from 'react';
import { SafeAreaView, TouchableOpacity, View, Text, StyleSheet, Image } from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';
import { ContactContext } from '../context/contactContext';
import LoadingComponent from '../components/loadingComponent';
import HeaderComponent from '../components/headerComponent';
import { styles } from '../styles/style';


const ListContact = ({ navigation }) => {
  const [isLoading, setIsLoading] = useState(true);
  const { data, setData } = useContext(ContactContext);
  console.log(data)

  const handleDelete = (id) => {
    const updateData = data.filter((item) => item.id !== id);
    setData(updateData);
  };

  const handleUpdate = (id) => {
    const contactToUpdate = data.find((item) => item.id === id);
    if (contactToUpdate) {
      navigation.navigate('Edit Contact', { contact: contactToUpdate });
    }
  };

  const renderItem = ({ item }) => (
    <View style={styles.itemContainer}>
      {item.imageSource !== '' ? (
        <Image source={{ uri: item.imageSource }} style={styles.avatarPlaceholder} />
      ) : (
        <View style={styles.avatarPlaceholder}>
          <Text style={styles.placeholderText}>N/A</Text>
        </View>
      )}
      <View style={styles.textContainer}>
        <Text style={styles.name}>{item.fullname}</Text>
        <Text style={styles.phone}>{item.phonenumber}</Text>
      </View>
    </View>
  );

  const renderHiddenItem = (data) => (
    <View style={styles.hiddenItemContainer}>
      <TouchableOpacity style={{ backgroundColor: '#3498db', justifyContent: 'center', alignItems: 'center', width: 47 }} onPress={() => handleUpdate(data.item.id)}>
        <Text style={{ color: 'white' }}>Edit</Text>
      </TouchableOpacity>
      <TouchableOpacity style={{ backgroundColor: '#e74c3c', justifyContent: 'center', alignItems: 'center', width: 47 }} onPress={() => handleDelete(data.item.id)}>
        <Text style={{ color: 'white' }}>Delete</Text>
      </TouchableOpacity>
    </View>
  );

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 2000);
  }, []);

  if (isLoading) {
    return (
      <LoadingComponent/>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
        <HeaderComponent/>
        <View style={styles.container}>
              {data !== '' ? (
                <SwipeListView
                    data={data}
                    renderItem={renderItem}
                    renderHiddenItem={renderHiddenItem}
                    rightOpenValue={-95}
                />
              ) : (
                <View style={styles.textContainer}>
                  <Text style={styles.nodata}>Not Have Data</Text>
                </View>
              )}
            <TouchableOpacity style={styles.addButton} onPress={() => navigation.navigate('Add Contact')}>
                <Text style={styles.addButtonText}>+</Text>
            </TouchableOpacity>
        </View>
    </SafeAreaView>
  );
};

export default ListContact;